#!/bin/sh

if [ $0 != "scripts/production_update.sh" ]; then
	echo " Please start this from itswebsite2 root folder."
	echo " Usage: sh scripts/production_update.sh"
	exit 1
fi

git pull origin master
RAILS_ENV=production bundle exec rake assets:clean
RAILS_ENV=production bundle exec rake assets:precompile
chown -R www-data:www-data .
service puma-manager stop
service puma-manager start
service nginx restart
