rails_env = ENV['RAILS_ENV'] || 'development'
app_path = '/srv/www/www.itsium.cn/itswebsite2'
directory app_path
ENV["HOME"] = app_path

threads 4,4

bind  "unix://#{app_path}/tmp/sockets/itswebsite2.sock"
pidfile "#{app_path}/tmp/pids/puma.pid"
state_path "#{app_path}/tmp/sockets/puma.state"

activate_control_app
