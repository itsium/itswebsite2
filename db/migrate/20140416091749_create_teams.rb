class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.string :position
      t.string :email
      t.string :phone
      t.string :description

      t.timestamps
    end
  end
end
