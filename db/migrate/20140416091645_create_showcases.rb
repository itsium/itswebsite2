class CreateShowcases < ActiveRecord::Migration
  def change
    create_table :showcases do |t|
      t.string :title
      t.string :subtitle
      t.string :link
      t.string :description
      t.string :short_description
      t.integer :sctype

      t.timestamps
    end
  end
end
