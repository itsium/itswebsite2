class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :pagename
      t.string :title
      t.string :keyword
      t.text :description
      t.string :author
      t.text :headhtml
      t.text :bodyhtml

      t.timestamps
    end
  end
end
