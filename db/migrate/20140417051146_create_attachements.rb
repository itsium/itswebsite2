class CreateAttachements < ActiveRecord::Migration
  def change
    create_table :attachements do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
