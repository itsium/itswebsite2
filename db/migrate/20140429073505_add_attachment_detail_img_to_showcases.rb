class AddAttachmentDetailImgToShowcases < ActiveRecord::Migration
  def self.up
    change_table :showcases do |t|
      t.attachment :detail_img
    end
  end

  def self.down
    drop_attached_file :showcases, :detail_img
  end
end
