class AddOrdernumToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :ordernum, :integer, :default => 99
  end
end
