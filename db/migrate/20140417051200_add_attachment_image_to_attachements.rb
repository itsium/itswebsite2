class AddAttachmentImageToAttachements < ActiveRecord::Migration
  def self.up
    change_table :attachements do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :attachements, :image
  end
end
