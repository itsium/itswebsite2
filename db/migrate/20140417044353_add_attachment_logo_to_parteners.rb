class AddAttachmentLogoToParteners < ActiveRecord::Migration
  def self.up
    change_table :parteners do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :parteners, :logo
  end
end
