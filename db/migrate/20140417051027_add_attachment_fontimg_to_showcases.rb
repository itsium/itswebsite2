class AddAttachmentFontimgToShowcases < ActiveRecord::Migration
  def self.up
    change_table :showcases do |t|
      t.attachment :fontimg
    end
  end

  def self.down
    drop_attached_file :showcases, :fontimg
  end
end
