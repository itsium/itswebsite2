class CreateParteners < ActiveRecord::Migration
  def change
    create_table :parteners do |t|
      t.string :name
      t.string :link

      t.timestamps
    end
  end
end
