class AddShowcaseIdToAttachement < ActiveRecord::Migration
  def change
    add_reference :attachements, :showcase, index: true
  end
end
