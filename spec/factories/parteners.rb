# == Schema Information
#
# Table name: parteners
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  link              :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :partener do
    name "MyString"
    link "MyString"
  end
end
