# == Schema Information
#
# Table name: settings
#
#  id          :integer          not null, primary key
#  pagename    :string(255)
#  title       :string(255)
#  keyword     :string(255)
#  description :text
#  author      :string(255)
#  headhtml    :text
#  bodyhtml    :text
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :setting do
    pagename "MyString"
    title "MyString"
    keyword "MyString"
    description "MyText"
    author "MyString"
    headhtml "MyText"
    bodyhtml "MyText"
  end
end
