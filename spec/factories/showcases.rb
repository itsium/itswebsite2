# == Schema Information
#
# Table name: showcases
#
#  id                   :integer          not null, primary key
#  title                :string(255)
#  subtitle             :string(255)
#  link                 :string(255)
#  description          :string(255)
#  short_description    :string(255)
#  sctype               :integer
#  created_at           :datetime
#  updated_at           :datetime
#  fontimg_file_name    :string(255)
#  fontimg_content_type :string(255)
#  fontimg_file_size    :integer
#  fontimg_updated_at   :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :showcase do
    title "MyString"
    subtitle "MyString"
    link "MyString"
    description "MyString"
    short_description "MyString"
    sctype 1
  end
end
