class ContactMailer < ActionMailer::Base
  default from: "itsium.mailler@gmail.com"

  def contact_mail(p)
    @user = p[:name]
    @mail = p[:mail]
    @body = p[:body]
    mail to: "contact@itsium.cn", subject: "[ITSIUM_WEBSITE] - #{p[:mail]}"
  end

end
