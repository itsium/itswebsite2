(function ($) {

    $(function () {

        var navbarHeight = 80,
            win = window,
            $win = $(win);

        // Slide in Functionality
        $win.scroll(function () {
            var top = win.scrollY;
            $(".slide-in").each(function () {
                var thisTop = $(this).offset().top;
                var height = $(this).height();
                if ((top > (thisTop - (height * 1.5))) && !$(this).hasClass("slid")) {
                    $(this).addClass("slid");
                }
            });
        });

        $('body').scrollspy({
            offset: navbarHeight + 10,
            target: '#navbar'
        });

        // Home
        $('.carousel').carousel({
            pause: false,
            interval: 8000
        });

        // Navbar Affix
        $('#navbar').affix({
          offset: {
            top: function () {
              return (this.top = $win.height() - navbarHeight)
            }
          }
        });

        // function affix(el, topFunc) {

        //     // affix-top when must keep at bottom of page
        //     // affix when must keep at top of page

        //     var top = topFunc(),
        //         is_top = true,
        //         has_moved = false,
        //         $el = $(el);

        //     var checkPosition = function () {

        //         if (!has_moved) return;
        //         has_moved = false;

        //         var must_down = (win.scrollY > top);

        //         if (is_top !== false && must_down === true) {
        //             is_top = false;
        //             $el.addClass('affix');
        //         } else if (is_top !== true && must_down === false) {
        //             is_top = true;
        //             $el.removeClass('affix');
        //         }

        //     };

        //     var sid = setInterval(checkPosition, 150);

        //     if (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch)) {
        //         win.addEventListener('touchmove', function () {
        //             has_moved = true;
        //         }, false);
        //     } else {
        //         $win.on('scroll', function () {
        //             has_moved = true;
        //         });
        //     }
        //     $win.on('resize', function () {
        //         top = topFunc();
        //         has_moved = true;
        //     });

        //     setTimeout(function() {
        //         has_moved = true;
        //     }, 1000);

        // }

        // affix(document.getElementById('navbar'), function () {
        //     return (win.innerHeight - navbarHeight);
        // });



      function mapInitialize(){
        mapObj = new AMap.Map("map-canvas",{
          center:new AMap.LngLat(116.349056,40.006914), //地图中心点
          level:13,  //地图显示的缩放级别
          dragEnable:false,
          keyboardEnable:false,
          scrollWheel:false
        });
          marker=new AMap.Marker({
          position:new AMap.LngLat(116.349056,40.006914)

          });
          marker.setMap(mapObj);  //在地图上添加点
          var infoWindow = new AMap.InfoWindow({
              content:"<h4><font color=\"#00a6ac\">&nbsp;&nbsp;" + "北京意得捷优科技有限公司" +"</font></h4>"+
                "&nbsp;&nbsp;地址： 北京市海淀区静淑苑路2号（创业广场601A）<br />" +
                "&nbsp;&nbsp;电话：<a href=\"tel:010-82363688\">010-82363688</a><br />",
              autoMove:true,
              size:new AMap.Size(250,0),
              offset:{x:0,y:-30}
          });
          var aa = function(e){infoWindow.open(mapObj,marker.getPosition());};
          AMap.event.addListener(marker,"click",aa)
      }
      mapInitialize();

        var homeLogo = $('#home .logo'),
            homeItems = $("#home .logo, #home .welcome, #home .call-to-action, #home .macbook-preview");

        // Parallax Scripts
        function updateParallax() {
            if (win.innerWidth > 768) {
                $(".parallax").each(function () {
                    var bottom = $(this).offset().top + $(this).height();
                    var top = $(this).offset().top;
                    var windowHeight = win.innerHeight;
                    var scrollTop = win.scrollY + windowHeight;
                    var fromTop = 0;
                    var isHome = true;
                    if (top === 0) {
                        fromTop = win.scrollY - top;
                        isHome = true;
                    } else {
                        fromTop = win.scrollY - top + windowHeight;
                        isHome = false;
                    }
                    if ((bottom > win.scrollY) && (top < scrollTop)) {
                        var parallax = -1 * (fromTop / 3);
                        var revParallax = parallax;
                        var percent = 1 - 1.3 * (win.scrollY / windowHeight);
                        if (isHome) {
                            revParallax += navbarHeight;
                            homeLogo.css('marginTop', parallax + "px");
                            homeItems.css('opacity', percent);
                        }
                        $(this).children("img").first().css('bottom', revParallax + "px");
                    }
                });
            }
        }
        updateParallax();

        $win.scroll(updateParallax);


        // Contact Form Icon
        $("form .form-control").focus(function () {
            $(this).siblings("label").first().children("i").first().css({
                "color": "#aaa",
                "left": 0
            });
        });
        $("form .form-control").blur(function () {
            $(this).siblings("label").first().children("i").first().css({
                "color": "transparent",
                "left": "-20px"
            });
        });

        // Blog Masonry
        var $container = $('.masonry-grid');

        $container.imagesLoaded(function () {

            new AnimOnScroll(document.getElementById('grid'), {
                minDuration: 0.4,
                maxDuration: 0.7,
                viewportFactor: 0.2
            });

            // Smooth Scrolling
            $("a.scroll").click(function (e) {
                e.preventDefault();
                var offset = $(this.hash).offset().top - (navbarHeight / 2);
                $('html, body').animate({
                    scrollTop: offset
                }, 600);
            });

        });

        $(document).on('show.bs.modal', '.modal-portfolio', function () {

            var modal = $(this);

            setTimeout(function() {
                height = modal.find('.wrapper .modal-app .row').height();
                if (!height) return false;
                modal.find('.infos').height((height - 60));
            }, 350);
        });

        $('a.scroll').click(function() {

            var $navbar = $('[data-toggle=collapse]'),
                $target = $($navbar.attr('data-target'));

            if ($target) {
                $target.removeClass('in');
                $target.addClass('collapse');
            }

        });

        // $('[data-toggle="collapse"]').click(function() {

        //     var $target = $($(this).data('target'));

        //     if(!$target.hasClass('in'))
        //         $('.container .in').removeClass('in').height(0);
        // });

    }); // end of document ready

})(jQuery);
