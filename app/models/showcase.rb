# == Schema Information
#
# Table name: showcases
#
#  id                   :integer          not null, primary key
#  title                :string(255)
#  subtitle             :string(255)
#  link                 :string(255)
#  description          :string(255)
#  short_description    :string(255)
#  sctype               :integer
#  created_at           :datetime
#  updated_at           :datetime
#  fontimg_file_name    :string(255)
#  fontimg_content_type :string(255)
#  fontimg_file_size    :integer
#  fontimg_updated_at   :datetime
#

class Showcase < ActiveRecord::Base
    extend Enumerize
    has_attached_file :fontimg, :styles => { :original => '580x370#' }, :default_url => "/images/:style/fontimg-missing.png"
    has_attached_file :detail_img, :styles => { :website => '854x532#', app: '323x570#' }, :default_url => "/images/:style/detail_img-missing.png"
    validates_attachment_content_type :fontimg, :content_type => /\Aimage\/.*\Z/
    validates_attachment_content_type :detail_img, :content_type => /\Aimage\/.*\Z/
    has_many :showcases, dependent: :destroy
    validates_attachment_presence :fontimg
    validates_attachment_size :fontimg, less_than: 5.megabytes
    validates_attachment_size :detail_img, less_than: 5.megabytes
    validates_attachment_presence :detail_img

    enumerize :sctype, in: {:app => 1, :website => 2, :iframe => 3, :other => 4}
end
