# == Schema Information
#
# Table name: attachements
#
#  id                 :integer          not null, primary key
#  title              :string(255)
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  showcase_id        :integer
#

class Attachement < ActiveRecord::Base
  belongs_to :showcase
  has_attached_file :image, :styles => { :original => '400x400>' }, :default_url => "/images/:style/image-missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
