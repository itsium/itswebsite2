# == Schema Information
#
# Table name: parteners
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  link              :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#

class Partener < ActiveRecord::Base
  has_attached_file :logo, :styles => { :original => '100x30>' }, :default_url => "/images/:style/logo-missing.png"
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  validates_attachment_presence :logo
end
