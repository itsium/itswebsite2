# == Schema Information
#
# Table name: teams
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  position           :string(255)
#  email              :string(255)
#  phone              :string(255)
#  description        :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#

class Team < ActiveRecord::Base
    has_attached_file :photo, :styles => { :original => '400x400>' }, :default_url => "/images/:style/photo-missing.png"
    validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
    validates_attachment_presence :photo
    validates_attachment_size :photo, less_than: 5.megabytes
end
