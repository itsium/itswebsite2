# == Schema Information
#
# Table name: settings
#
#  id          :integer          not null, primary key
#  pagename    :string(255)
#  title       :string(255)
#  keyword     :string(255)
#  description :text
#  author      :string(255)
#  headhtml    :text
#  bodyhtml    :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Setting < ActiveRecord::Base
end
