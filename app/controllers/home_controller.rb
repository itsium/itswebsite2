class HomeController < ApplicationController
  def index
    @parteners = Partener.all
    @showcases = Showcase.order(:ordernum)
    @team = Team.all
  end

  def contact_mail
    ContactMailer.contact_mail(params).deliver
    redirect_to :back
  end
end
