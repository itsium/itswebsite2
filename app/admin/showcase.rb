ActiveAdmin.register Showcase do
  permit_params :title, :subtitle, :link, :link2, :description, :short_description, :sctype, :fontimg, :detail_img, :ordernum

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  form do |f|
    f.inputs t(:showcase_detail) do
      f.input :ordernum
      f.input :title
      f.input :subtitle
      f.input :link
      f.input :link2
      f.input :description
      f.input :short_description
      f.input :sctype, as: :select
      f.input :fontimg, as: :file, hint: f.template.image_tag(f.object.fontimg.url(:original))
      if f.object.sctype and f.object.sctype.app?
        f.input :detail_img, as: :file, hint: f.template.image_tag(f.object.detail_img.url(:app))
      else
        f.input :detail_img, as: :file, hint: f.template.image_tag(f.object.detail_img.url(:website))
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :ordernum
      row :subtitle
      row :link
      row :description
      row :short_description
      row :sctype
      row t(:fontimg) do |sc|
        image_tag(sc.fontimg.url(:original))
      end
      row t(:detail_img) do |sc|
        if sc.sctype.app?
          image_tag(sc.detail_img.url(:app))
        else
          image_tag(sc.detail_img.url(:website))
        end
      end
    end
  end

  index do
    selectable_column
     column :id
     column :title
     column :ordernum
     column :subtitle
     column :link
    column :description
    column :short_description
    column :sctype
    column t(:fontimg_file_name) do |sc|
      link_to sc.fontimg_file_name, "#", data_img: sc.fontimg.url(:original)
    end
    column t(:detail_img_file_name) do |sc|
      if sc.sctype.app?
        link_to sc.detail_img_file_name, "#", data_img: sc.detail_img.url(:app)
      else
        link_to sc.detail_img_file_name, "#", data_img: sc.detail_img.url(:website)
      end
    end
    default_actions
  end

end

