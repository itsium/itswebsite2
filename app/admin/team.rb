ActiveAdmin.register Team do
  permit_params :name, :position, :email, :phone, :description, :photo

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

   form do |f|
    f.inputs t(:team_detail) do
      f.input :name
      f.input :position
      f.input :email
      f.input :phone
      f.input :description
      f.input :photo, as: :file, hint: f.template.image_tag(f.object.photo.url(:original))
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :position
      row :email
      row :phone
      row :description
      row t(:photo) do |sc|
        image_tag(sc.photo.url(:original))
      end
    end
  end

  index do
    selectable_column
     column :name
      column :position
      column :email
      column :phone
      column :description
      column t(:photo) do |sc|
       image_tag(sc.photo.url(:original), height: '128')
      end
    default_actions
  end

end
