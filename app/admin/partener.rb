ActiveAdmin.register Partener do
  actions :all, :except => [:show]
  permit_params :logo, :link, :name

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  # action_item :only => :show do
  #   link_to(I18n.t(:new_partener), new_admin_partener_url)
  # end


  form do |f|
    f.inputs I18n.t(:partener_detail) do
      f.input :name
      f.input :link
      f.input :logo, as: :file, :hint => f.template.image_tag(f.object.logo.url(:original))
    end
    f.actions
  end

  index do
    selectable_column
    column :id
    column :name
    column :link
    column :logo_file_name
          column I18n.t(:logo) do | partener |
        image_tag(partener.logo.url(:original))
      end
    default_actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :link
      row :logo_file_name
      row I18n.t(:logo) do | partener |
        image_tag(partener.logo.url(:original))
      end
    end
  end

end
